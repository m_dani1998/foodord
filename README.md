**Food Ordering android application with web scrapper**

Project synopsis:
An android application which will be able to order food from restaurant, and the data of the restaurant will be provided through a web scrapper.

Technologies:
Node.js(backend)
Android Java(FrontEnd)
WebScrapper(Scrappy & Selinium)